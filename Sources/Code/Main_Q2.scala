import com.tncy.top.image.ImageWrapper
import display.ListBuff.printListDetectedPatterns
import grayscale.Conversion.getGrayLevelValues
import grayscale.Highlight.highlightPatterns
import grayscale.PixelByPixel.detectPatterns

object Main_Q2 extends App {

  // Source image
  val sampleImage = "test/test01/image.png"
  val wrappedSample = new ImageWrapper(sampleImage)
  val sampleRGBPixels = wrappedSample.getImage()


  // Pattern image
  val patternImage = "test/test01/imotif.png"
  val wrappedPattern = new ImageWrapper(patternImage)
  val patternRGBPixels = wrappedPattern.getImage()


  // Get gray level values
  val sampleGrayPixels = getGrayLevelValues(sampleRGBPixels)
  val patternGrayPixels = getGrayLevelValues(patternRGBPixels)


  // Detect all the patterns in source image
  val detectedPatterns = detectPatterns(sampleGrayPixels, patternGrayPixels)
  printListDetectedPatterns(detectedPatterns)


  // Highlight patterns found
  highlightPatterns(sampleImage, detectedPatterns, "outputs/Output_Q2/highlightedPattern.png")

}
