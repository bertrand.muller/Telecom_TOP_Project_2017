import grayscale.Conversion.getGrayLevelValues
import com.tncy.top.image.ImageWrapper
import grayscale.Histograms.getHistogramValues
import grayscale.Histograms.displayHistogram

object Main_Q4 extends App {

  // Source image
  val sampleImage = "test/test01/image.png"
  val wrappedSample = new ImageWrapper(sampleImage)
  val sampleRGBPixels = wrappedSample.getImage()


  // Pattern image
  val patternImage = "test/test01/imotif.png"
  val wrappedPattern = new ImageWrapper(patternImage)
  val patternRGBPixels = wrappedPattern.getImage()


  // Get gray level values
  val sampleGrayPixels = getGrayLevelValues(sampleRGBPixels)
  val patternGrayPixels = getGrayLevelValues(patternRGBPixels)


  // Get (normalized) histogram values for each pixel
  val grayscaleHistogram = getHistogramValues(sampleGrayPixels, normalized = false)
  var grayscaleHistogramNormalized = getHistogramValues(sampleGrayPixels, normalized = true)


  // Display curved/bars histogram for grayscale values (not normalized)
  displayHistogram(grayscaleHistogram, curved = true)
  displayHistogram(grayscaleHistogram, curved = false)


  // Display curved/bars histogram for grayscale values (normalized)
  displayHistogram(grayscaleHistogramNormalized, curved = true)
  displayHistogram(grayscaleHistogramNormalized, curved = false)
  
}
