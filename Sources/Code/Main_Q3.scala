import com.tncy.top.image.ImageWrapper
import rgb.PixelByPixel.detectPatterns
import rgb.GetColor.getRGBLevelsValues
import rgb.Highlight.highlightPatterns
import display.ListBuff.printListDetectedPatterns

object Main_Q3 extends App {

  // Source image
  val sampleImage = "test/test12/image.png"
  val wrappedSample = new ImageWrapper(sampleImage)
  val sampleRGBPixels = wrappedSample.getImage()


  // Pattern image
  val patternImage = "test/test12/imotif.png"
  val wrappedPattern = new ImageWrapper(patternImage)
  val patternRGBPixels = wrappedPattern.getImage()


  // Get color level values
  val sampleColorPixels = getRGBLevelsValues(sampleRGBPixels)
  val patternColorPixels = getRGBLevelsValues(patternRGBPixels)


  // Detect all the patterns in source image
  val detectedPatterns = detectPatterns(sampleColorPixels, patternColorPixels)
  printListDetectedPatterns(detectedPatterns)


  // Highlight patterns found
  highlightPatterns(sampleImage, detectedPatterns, "outputs/Output_Q3/multipleHighlightedPatterns.png")

}
