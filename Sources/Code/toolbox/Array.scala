package toolbox

object Array extends App {

  /**
    * Used to duplicate an array of array of integer values
    * @param source Array to duplicate
    * @param destination Duplicated array
    */
  def copy(source: Array[Array[Int]], destination: Array[Array[Int]]):Unit = {
    for (i <- source.indices) {
      for(j <- source(i).indices) {
        destination(i)(j) = source(i)(j)
      }
    }
  }


  /**
    * Used to duplicate an array of array of double values
    * @param source Array to duplicate
    * @param destination Duplicated array
    */
  def copy(source: Array[Array[Double]], destination: Array[Array[Double]]):Unit = {
    for (i <- source.indices) {
      for(j <- source(i).indices) {
        destination(i)(j) = source(i)(j)
      }
    }
  }


  /**
    * Used to convert an array of integers to an array of doubles
    * @param source Array of integers to convert
    * @return Array of integers converted to doubles
    */
  def convertToArrayDouble(source: Array[Int]):Array[Double] = {
    var destination = new Array[Double](source.length)
    for(i <- source.indices) {
      destination(i) = source(i).asInstanceOf[Double]
    }
    return destination
  }


  /**
    * Used to convert an array of array of integers to an array of array of doubles
    * @param source Array of array of integers to convert
    * @return Array of array of integers converted to doubles
    */
  def convertToArrayDouble(source: Array[Array[Int]]):Array[Array[Double]] = {
    var destination = scala.Array.ofDim[Double](source.length,source(0).length)
    for(i <- source.indices) {
      for(j <- source(i).indices) {
        destination(i)(j) = source(i)(j).asInstanceOf[Double]
      }
    }
    return destination
  }

}
