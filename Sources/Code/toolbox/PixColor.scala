package toolbox

import caseClasses.PixelColor

object PixColor extends App {


  /**
    * Used to compare two pixels accoridng to their Red, Green & Blue values
    * @param pixColor1 First pixel
    * @param pixColor2 Second pixel
    * @return Indicate if the pixels are identical
    */
  def comparePixelColor(pixColor1: PixelColor, pixColor2: PixelColor):Boolean = {

    var identicalRed = (pixColor1.red == pixColor2.red)
    var identicalGreen = (pixColor1.green == pixColor2.green)
    var identicalBlue = (pixColor1.blue == pixColor2.blue)

    return (identicalRed && identicalGreen && identicalBlue)

  }

}
