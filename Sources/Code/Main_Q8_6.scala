import com.tncy.top.image.ImageWrapper
import grayscale.IntegralHistograms.getSummedAreaTableHorizontal
import grayscale.IntegralHistograms.getSummedAreaTableVertical
import grayscale.IntegralHistograms.detecteIntegralHistogram
import grayscale.Conversion.getGrayLevelValues
import grayscale.IntegralHistograms.getSummedAreaTable

object Main_Q8_6 extends App {

  // Source image
  val sampleImage = "sampleImage.png"
  val wrappedSample = new ImageWrapper(sampleImage)
  val sampleRGBPixels = wrappedSample.getImage()


  // Pattern image
  val patternImage = "patternImage.png"
  val wrappedPattern = new ImageWrapper(patternImage)
  val patternRGBPixels = wrappedPattern.getImage()

  // Get gray level values
  val sampleGrayPixels = getGrayLevelValues(sampleRGBPixels)
  val patternGrayPixels = getGrayLevelValues(patternRGBPixels)


  // Get Integral histogram grayscale values for the source image
  val summedHorizontalSource = getSummedAreaTableHorizontal(sampleGrayPixels)
  val summedVerticalSource = getSummedAreaTableVertical(sampleGrayPixels)

  val detecte = detecteIntegralHistogram(sampleGrayPixels,summedHorizontalSource,summedVerticalSource,patternGrayPixels)

}
