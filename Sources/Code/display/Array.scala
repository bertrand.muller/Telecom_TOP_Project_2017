package display

object Array extends App {

  /**
    * Used to print a two-dimensionnal integer array in the console
    * @param array Array to print
    */
  def printTwoDimensionnalArray(array: Array[Array[Int]]):Unit = {
    for(i <- array.indices) {
      for(j <- array(i).indices) {
        print(array(i)(j) + " ")
      }
      println
    }
    println
  }

}
