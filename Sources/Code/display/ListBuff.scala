package display

import caseClasses.DetectedPattern

import scala.collection.mutable.ListBuffer

object ListBuff extends App {

  /**
    * Used to print a list of detected patterns (grayscale)
    * @param listPatterns List to print
    */
  def printListDetectedPatterns(listPatterns: ListBuffer[DetectedPattern]):Unit = {
    listPatterns.foreach(println)
  }

}
