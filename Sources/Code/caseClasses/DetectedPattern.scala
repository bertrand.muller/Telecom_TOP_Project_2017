package caseClasses

case class DetectedPattern(topLeftX: Int, topLeftY: Int, bottomRightX: Int, bottomRightY: Int)
