package caseClasses

case class PixelColor(red: Int, green: Int, blue: Int)
