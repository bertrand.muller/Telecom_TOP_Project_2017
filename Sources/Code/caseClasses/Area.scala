package caseClasses

case class Area(pixel: Pixel, reversed: Boolean)
