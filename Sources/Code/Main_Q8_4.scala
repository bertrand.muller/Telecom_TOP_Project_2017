import com.tncy.top.image.ImageWrapper
import rgb.GetColor.getRGBLevelsValues
import rgb.Histograms.getHistogramValues
import rgb.Histograms.displayHistogram

object Main_Q8_4 extends App {

  // Source image
  val sampleImage = "test/test01/image.png"
  val wrappedSample = new ImageWrapper(sampleImage)
  val sampleRGBPixels = wrappedSample.getImage()


  // Pattern image
  val patternImage = "test/test01/imotif.png"
  val wrappedPattern = new ImageWrapper(patternImage)
  val patternRGBPixels = wrappedPattern.getImage()


  // Get color level values
  val sampleColorPixels = getRGBLevelsValues(sampleRGBPixels)
  val patternColorPixels = getRGBLevelsValues(patternRGBPixels)


  // Get (normalized) histogram color values for each pixel
  val colorHistograms = getHistogramValues(sampleColorPixels, normalized = false)
  val colorHistogramsNormalized = getHistogramValues(sampleColorPixels, normalized = true)


  // Display curved/bars histogram for grayscale values (not normalized)
  displayHistogram(colorHistograms, curved = true)
  displayHistogram(colorHistograms, curved = false)


  // Display curved/bars histogram for grayscale values (normalized)
  displayHistogram(colorHistogramsNormalized, curved = true)
  displayHistogram(colorHistogramsNormalized, curved = false)

}
