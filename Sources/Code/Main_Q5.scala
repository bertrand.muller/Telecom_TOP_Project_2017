import com.tncy.top.image.ImageWrapper
import display.ListBuff.printListDetectedPatterns
import grayscale.Conversion.getGrayLevelValues
import grayscale.Histograms.{detectPatternsByHistograms, getAllHistogramsPixelMatchPattern, getHistogramValues}
import grayscale.Highlight.highlightPatterns

object Main_Q5 extends App {

  // Source image
  val sampleImage = "test/test02/image.png"
  val wrappedSample = new ImageWrapper(sampleImage)
  val sampleRGBPixels = wrappedSample.getImage()


  // Pattern image
  val patternImage = "test/test02/imotif.png"
  val wrappedPattern = new ImageWrapper(patternImage)
  val patternRGBPixels = wrappedPattern.getImage()


  // Get gray level values
  val sampleGrayPixels = getGrayLevelValues(sampleRGBPixels)
  val patternGrayPixels = getGrayLevelValues(patternRGBPixels)


  // Get normalized histogram grayscale values for the pattern image
  val grayscaleHistogramNormalizedPattern = getHistogramValues(patternGrayPixels, normalized = true)


  // Get all the normalized histograms for the areas of the source image
  // which first pixel corresponds to the one of the corner pixel of the pattern
  val histogramsFirstPixelPattern = getAllHistogramsPixelMatchPattern(sampleGrayPixels, patternGrayPixels, normalized = true)

  // Search detected patterns with Bhattacharyya Method
  val detectedPatterns = detectPatternsByHistograms(histogramsFirstPixelPattern, grayscaleHistogramNormalizedPattern, patternGrayPixels(0).length, patternGrayPixels.length, 1)
  printListDetectedPatterns(detectedPatterns)


  // Highlight patterns found with Bhattacharyya Method
  highlightPatterns(sampleImage, detectedPatterns, "outputs/Output_Q5/sampleHighlighted_Histograms_Bhatta.png")


  // Search detected patterns with Intersection Method
  val detectedPatterns2 = detectPatternsByHistograms(histogramsFirstPixelPattern, grayscaleHistogramNormalizedPattern, patternGrayPixels(0).length, patternGrayPixels.length, 2)


  // Highlight detected patterns with Intersection Method
  highlightPatterns(sampleImage, detectedPatterns2, "outputs/Output_Q5/sampleHighlighted_Histograms_Intersection.png")


  // Search detected patterns with Chi-Square Method
  val detectedPatterns3 = detectPatternsByHistograms(histogramsFirstPixelPattern, grayscaleHistogramNormalizedPattern, patternGrayPixels(0).length, patternGrayPixels.length, 3)


  // Highlight detected patterns with Chi-Square Method
  highlightPatterns(sampleImage, detectedPatterns3, "outputs/Output_Q5/sampleHighlighted_Histograms_ChiSquare.png")

  // Search detected patterns with Correlation Method
  val detectedPatterns4 = detectPatternsByHistograms(histogramsFirstPixelPattern, grayscaleHistogramNormalizedPattern, patternGrayPixels(0).length, patternGrayPixels.length, 4)


  // Highlight detected patterns with Correlation Method
  highlightPatterns(sampleImage, detectedPatterns4, "outputs/Output_Q5/sampleHighlighted_Histograms_Correlation.png")

}
