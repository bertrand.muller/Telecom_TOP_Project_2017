import com.tncy.top.image.ImageWrapper
import rgb.GetColor.getRGBLevelsValues
import rgb.Histograms.{detectPatternsByHistograms, getHistogramValues, getAllHistogramsPixelMatchPattern}
import display.ListBuff.printListDetectedPatterns
import rgb.Highlight.highlightPatterns

object Main_Q8_5 extends App {

  // Source image
  val sampleImage = "test/test01/image.png"
  val wrappedSample = new ImageWrapper(sampleImage)
  val sampleRGBPixels = wrappedSample.getImage()


  // Pattern image
  val patternImage = "test/test01/imotif.png"
  val wrappedPattern = new ImageWrapper(patternImage)
  val patternRGBPixels = wrappedPattern.getImage()


  // Get color level values
  val sampleColorPixels = getRGBLevelsValues(sampleRGBPixels)
  val patternColorPixels = getRGBLevelsValues(patternRGBPixels)


  // Get normalized histogram color values for the pattern image
  val colorHistogramsNormalizedPattern = getHistogramValues(patternColorPixels, normalized = true)


  // Get all the normalized histograms for the areas of the source image
  // which first pixel corresponds to the one of the corner pixel of the pattern
  val histogramsFirstPixelPattern = getAllHistogramsPixelMatchPattern(sampleColorPixels, patternColorPixels, normalized = true)


  // Search detected patterns with Bhattacharyya Method
  val detectedPatterns = detectPatternsByHistograms(histogramsFirstPixelPattern, colorHistogramsNormalizedPattern, patternColorPixels(0).length, patternColorPixels.length, 1)
  printListDetectedPatterns(detectedPatterns)


  // Highlight patterns found with Bhattacharyya Method
  highlightPatterns(sampleImage, detectedPatterns, "outputs/output_Q8_5/sampleHighlighted_Histograms_Color_Bhatta.png")


  // Search detected patterns with Intersection Method
  val detectedPatterns2 = detectPatternsByHistograms(histogramsFirstPixelPattern, colorHistogramsNormalizedPattern, patternColorPixels(0).length, patternColorPixels.length, 2)


  // Highlight detected patterns with Intersection Method
  highlightPatterns(sampleImage, detectedPatterns2, "outputs/output_Q8_5/sampleHighlighted_Histograms_Color_Intersection.png")


  // Search detected patterns with Chi-Square Method
  val detectedPatterns3 = detectPatternsByHistograms(histogramsFirstPixelPattern, colorHistogramsNormalizedPattern, patternColorPixels(0).length, patternColorPixels.length, 3)


  // Highlight detected patterns with Chi-Square Method
  highlightPatterns(sampleImage, detectedPatterns3, "outputs/output_Q8_5/sampleHighlighted_Histograms_Color_ChiSquare.png")


  // Search detected patterns with Correlation Method
  val detectedPatterns4 = detectPatternsByHistograms(histogramsFirstPixelPattern, colorHistogramsNormalizedPattern, patternColorPixels(0).length, patternColorPixels.length, 4)


  // Highlight detected patterns with Correlation Method
  highlightPatterns(sampleImage, detectedPatterns4, "outputs/output_Q8_5/sampleHighlighted_Histograms_Color_Correlation.png")

}
