package grayscale


import com.tncy.top.image.ImageWrapper

object Conversion extends App {

  /**
    * Used to get a two-dimensional array containing the unique gray level value for each pixel
    * @param source Two-dimensionnal array with RGB values for each pixel
    * @return Two-dimensionnal array with gray values
    */
  def getGrayLevelValues(source: Array[Array[Int]]): Array[Array[Int]] = {

    val height = source.length
    val width = source(0).length
    val grayLevels:Array[Array[Int]] = Array.ofDim(height, width)

    for (h <- 0 until height) {
      for (w <- 0 until width) {
        grayLevels(h)(w) = getAverageGrayLevel(source, h, w)
      }
    }

    return grayLevels

  }


  /**
    * Used to get the average gray level from RGB values
    * @param source Source image
    * @param sourceHeight Current Y-Axis value for the pixel
    * @param sourceWidth Current X-Axis value for the pixel
    * @return Average gray level
    */
  def getAverageGrayLevel(source: Array[Array[Int]], sourceHeight: Int, sourceWidth: Int): Int = {

    val codeRGB: String = source(sourceHeight)(sourceWidth).toHexString

    if(!codeRGB.equals("0")) {
      val rgb = codeRGB takeRight 6
      val red = Integer.parseInt(rgb.substring(0,2), 16)
      val green = Integer.parseInt(rgb.substring(2,4), 16)
      val blue = Integer.parseInt(rgb.substring(4,6), 16)
      return (red + blue + green) / 3
    }

    return 255

  }


  /**
    * Used to save an image from grayscale values
    * Saved in the default directory
    * @param grayValues Two-dimensionnal array with gray level for each pixel
    * @param outputImage Name of the image to output
    * @return
    */
  def saveImageInGrayscale(sampleImageName: String, grayValues: Array[Array[Int]], outputImage: String) = {

    val sourceWrapper = new ImageWrapper(sampleImageName)
    val source = sourceWrapper.getImage()
    val height = grayValues.length
    val width = grayValues(0).length

    for (h <- 0 until height) {
      for (w <- 0 until width) {
        var grayLevel = grayValues(h)(w).toHexString
        if (grayLevel.length < 2) {
          grayLevel = "0".concat(grayLevel)
        }
        source(h)(w) = Integer.parseInt(grayLevel.concat(grayLevel).concat(grayLevel), 16)
      }
    }

    sourceWrapper.saveImage(outputImage)

  }

}
