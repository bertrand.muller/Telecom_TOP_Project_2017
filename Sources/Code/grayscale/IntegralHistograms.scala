package grayscale

import caseClasses.{DetectedPattern, Pixel}
import toolbox.Array.copy
import toolbox.Array.convertToArrayDouble

import scala.collection.mutable.ListBuffer

object IntegralHistograms extends App {

  /**
    * Used to calculate the summed area value of a given pixel
    * @param summedArea Summed Area Table to modify
    * @param x x-coordinate of the pixel
    * @param y y-coordinate of the pixel
    * @return Summed Area Table modified
    */
  def getSummedAreaValue(summedArea: Array[Array[Double]], x: Int, y: Int):Double = {

    var res = summedArea(y)(x)

    // Pixel on the left
    if(x-1 >= 0) {
      res += summedArea(y)(x-1)
    }

    // Pixel at the top
    if(y-1 >= 0) {
      res += summedArea(y-1)(x)
    }

    // Pixel on the top left
    if((x-1 >= 0) & (y-1 >= 0)) {
      res -= summedArea(y-1)(x-1)
    }

    return res

  }


  /**
    * Used to get the Summed Area Table for an image
    * @param imagePixels Pixels of the image
    * @return Summed Area Table for the given image
    */
  def getSummedAreaTable(imagePixels: Array[Array[Int]]):Array[Array[Double]] = {

   val imagePixelsNew = convertToArrayDouble(imagePixels)
   val height = imagePixels.length
   val width = imagePixels(0).length
   var summedArea = Array.ofDim[Double](height,width)

   // Duplicate array of pixels
   copy(imagePixelsNew,summedArea)

   // Create the Summed Area Table for each pixel
   for(i <- 0 until height) {
     for (j <- 0 until width) {
       summedArea(i)(j) = getSummedAreaValue(summedArea,j,i)
     }
   }

   return summedArea

 }


  def detectPatternNewMethod(summedArea: Array[Array[Double]], patternImage: Array[Array[Double]]):ListBuffer[DetectedPattern] = {

    val heightImagePattern = patternImage.length
    val widthImagePattern = patternImage(0).length
    val heightImageSource = summedArea.length
    val widthImageSource = summedArea(0).length

    var detectedPatterns = ListBuffer[DetectedPattern]()

    for (i <- 0 until heightImageSource - heightImagePattern) {
      for (j <- 0 until widthImageSource - widthImagePattern) {

        val pixelTopLeft = Pixel(j,i)
        val pixelBottomRight = Pixel(j+widthImagePattern, i+heightImagePattern)

        val histogramTopLeft = getIntegralHistogramZone(summedArea, pixelTopLeft.x-1, pixelTopLeft.y-1)
        val histogramLeft = getIntegralHistogramZone(summedArea, pixelBottomRight.x-widthImagePattern-1, pixelBottomRight.y)
        val histogramTop = getIntegralHistogramZone(summedArea, pixelBottomRight.x, pixelBottomRight.y-heightImagePattern-1)
        val histogramTarget = getIntegralHistogramZone(summedArea, pixelBottomRight.x, pixelBottomRight.y)

      }
    }

    return detectedPatterns

  }

  def compareIntegralHistogram(topLeft: Array[Array[Double]], left: Array[Array[Double]], top: Array[Array[Double]], zone: Array[Array[Double]]):Boolean = {

    val lengthTopLeft = topLeft.length
    val lengthLeft = left.length
    val lengthTop = top.length
    val max = Math.max(lengthLeft, Math.max(lengthTopLeft, lengthTop))

    val zoneHisto = new Array[Double](max)

    /*for(i <- zone.indices) {
      zoneHisto(i) =
    }*/

    return true

  }

  def getIntegralHistogramZone(summedArea: Array[Array[Double]], x: Int, y: Int):Array[Double] = {

    var tailleTableau = 0

    if(x > 0 && y > 0) {
      tailleTableau = x*y
    }

    var integralHistogram = new Array[Double](tailleTableau)
    var count = 0

    for(i <- 0 until y) {
      for(j <- 0 until x) {
        if(count != 0) {
          integralHistogram(count) = integralHistogram(count-1) + summedArea(i)(j)
        } else {
          integralHistogram(count) = summedArea(i)(j)
        }
        count += 1
      }
    }

    return integralHistogram

  }

 def getSummedAreaTableHorizontal(image: Array[Array[Int]]): Array[Array[Double]] = {

   val heightImage = image.length
   val widthImage = image(0).length
   var summedArea = Array.ofDim[Double](heightImage, widthImage)

   for (i <- 0 until heightImage) {
     for (j <- 0 until widthImage) {

       // Top left corner
       if(i == 0 && j == 0) {
         summedArea(i)(j) = image(i)(j)
       }

       // Not the first line but the first column
       if(i != 0 && j == 0) {
         summedArea(i)(j) = summedArea(i-1)(widthImage-1) + image(i)(j)
       }

       // Normal case
       if(j != 0) {
         summedArea(i)(j) = summedArea(i)(j-1) + image(i)(j)
       }

     }
   }

   return summedArea

 }


 def getSummedAreaTableVertical(image: Array[Array[Int]]): Array[Array[Double]] = {

   val heightImage = image.length
   val widthImage = image(0).length
   var summedArea = Array.ofDim[Double](heightImage, widthImage)

   for (j <- 0 until widthImage) {
     for (i <- heightImage-1 to 0 by -1) {

       // Bottom left corner
       if(i == heightImage-1 && j == 0) {
         summedArea(i)(j) = image(i)(j)
       }

       // Not the first column but the bottom line
       if(i == heightImage-1 && j != 0) {
         summedArea(i)(j) = summedArea(0)(j-1) + image(i)(j)
       }

       // Normal case
       if(i != heightImage-1) {
         summedArea(i)(j) = summedArea(i+1)(j) + image(i)(j)
       }

     }
   }

   return summedArea

 }

 def detecteIntegralHistogram(imageSource: Array[Array[Int]], Horizontal: Array[Array[Double]],Vertical: Array[Array[Double]], Pattern :  Array[Array[Int]]):ListBuffer[DetectedPattern] ={

   val heightImagePattern = Pattern.length
   val widthImagePattern = Pattern(0).length
   var summedArea = getSummedAreaTableHorizontal(Pattern)
   val LastPixel = summedArea(heightImagePattern-1)(widthImagePattern-1)

   val heightImageSource = Horizontal.length
   val widthImageSource = Horizontal(0).length

   var detectedPatterns = ListBuffer[DetectedPattern]()

   for (i <- 0 to heightImageSource - heightImagePattern -1) { // on fait le L Inversé
           for (j <- 0 to widthImageSource - widthImagePattern- 1) {// ON NE TRAITE PAS QUAND C'EST SUR UN BORD. J'ai pas le temps right now faut ajouter des if i = 0 et j = 0

             var A = Horizontal(i+heightImagePattern-1)(j+widthImagePattern-1) // coin en bas a droite dernier pixel de l'image pattern
             var B:Double = 0.0

               if(j == 0){
                 if(i != 0) {
                   B = Horizontal(i-1)(widthImageSource-1)
                 } else {
                   B = 0
                 }
               }else {
                 B = Horizontal(i)(j-1)// coin en haut à gauche juste avant image pattern
               }

             var C = Vertical(i)(j+widthImagePattern-1) // coin en haut à droite dernier pixel de pattern dans vertic
             var D = 0.0

             if (i+heightImagePattern-1 == heightImageSource-1){
               if(j != 0) {
                 D = Vertical(0)(j-1)
               } else {
                 D = 0
               }
             }else {
               D = Vertical(i+heightImagePattern)(j)// coin en bas à gauche en dessous de pattern
             }

             val E:Double = A-B
             val F:Double = C-D

             val arrayZone = getSummed(imageSource,j,i,widthImagePattern,heightImagePattern)
             val summedArrayZone = getSummedAreaTableHorizontal(arrayZone)

             var G:Double = (E+F) - Math.abs(E+F-2*summedArrayZone(heightImagePattern-1)(widthImagePattern-1))
             G = Math.abs(G)
             G = G/2
             if(G == LastPixel) {
               detectedPatterns += DetectedPattern(0,0,0,0)
             }
           }

     } // après faut faire péter la suite du code comme d'habitude genre on met les bords rouge etc...

     return detectedPatterns

 }

 def getSummed(imageSource: Array[Array[Int]], topLeftX: Int, topLeftY: Int, widthPattern: Int, heightPattern: Int):Array[Array[Int]] = {

   var array = Array.ofDim[Int](heightPattern, widthPattern)
   var countY = 0
   var countX = 0


   for(i <- topLeftY to heightPattern-1) {
     countX = 0
     for(j <- topLeftX to widthPattern-1) {
       array(countY)(countX) = imageSource(j)(i)
       countX += 1
     }
     countY += 1
   }

   return array

 }


}