package grayscale


import caseClasses.DetectedPattern
import com.tncy.top.image.ImageWrapper

import scala.collection.mutable.ListBuffer

object Highlight extends App {

  /**
    * Used to highlight one of the pattern detected in the source image
    * @param source Pixels of the source image
    * @param detectP Detected pattern to highlight
    */
  def highlightPattern(source: Array[Array[Int]], detectP: DetectedPattern):Unit = {

    val topLeftX = detectP.topLeftX
    val topLeftY = detectP.topLeftY
    var bottomRightX = detectP.bottomRightX
    var bottomRightY = detectP.bottomRightY

    if(bottomRightX >= source(0).length) { bottomRightX = source(0).length-1 }
    if(bottomRightY >= source.length) { bottomRightY = source.length-1 }

    // Top line
    for(tl <- topLeftX until bottomRightX) {
      source(topLeftY)(tl) = 0x00FF0000
    }

    // Left line
    for(ll <- topLeftY until bottomRightY) {
      source(ll)(topLeftX) = 0x00FF0000
    }

    // Bottom Line
    for(bl <- topLeftX until bottomRightX) {
      source(bottomRightY)(bl) = 0x00FF0000
    }

    // Right line
    for(rl <- topLeftY until bottomRightY) {
      source(rl)(bottomRightX) = 0x00FF0000
    }

  }


  /**
    * Used to highlight all the patterns detected in the source image
    * @param sampleImageName Name of the source image
    * @param detectedPatterns Detected patterns in the cource image
    * @param outputName Name of the output image with highlights
    * @return
    */
  def highlightPatterns(sampleImageName: String, detectedPatterns: ListBuffer[DetectedPattern], outputName: String) = {

    val sourceWrapper = new ImageWrapper(sampleImageName)
    val source = sourceWrapper.getImage()

    detectedPatterns.foreach(detectP => {
      highlightPattern(source, detectP)
    })

    sourceWrapper.saveImage(outputName)

  }

}
