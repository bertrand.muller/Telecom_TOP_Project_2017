package grayscale

import javafx.scene.paint.ImagePattern

import caseClasses.DetectedPattern
import com.tncy.top.image.ImageWrapper

import scala.collection.mutable.ListBuffer

object PixelByPixel extends App {

  /**
    * Used to test line by line if the pattern corresponds to the current part of the image
    * @param rowSrc Current row in the source image
    * @param colSrc Current column in the source image
    * @param rowPattern Current row in the pattern image
    * @param imageSrcPixels Grayscale values for all the pixels of the source image
    * @param imagePatternPixels Grayscale values for all the pixels of the pattern image
    * @return Indicate if the pattern is in the source image or not
    */
  def testPattern(rowSrc:Int, colSrc:Int, rowPattern:Int, imageSrcPixels:Array[Array[Int]], imagePatternPixels:Array[Array[Int]]):Boolean = {

    if (rowPattern > imagePatternPixels.length-1) {
      return true
    }

    // Compare values of the two images line/line
    // If there's any change, it can't be the pattern
    for (i <- imagePatternPixels(rowPattern).indices) {
      if (imageSrcPixels(rowSrc)(colSrc + i) != imagePatternPixels(rowPattern)(i)) {
        return false
      }
    }

    return testPattern(rowSrc + 1, colSrc, rowPattern + 1, imageSrcPixels, imagePatternPixels)

  }

  /**
    * Used to return a list of the patterns detected in the source image
    * @param srcPixels Pixels of the source image
    * @param patternPixels Pixels of the pattern image
    * @return A list of all the detected patterns in the source image (with their position)
    */
  def detectPatterns(srcPixels: Array[Array[Int]], patternPixels: Array[Array[Int]]):ListBuffer[DetectedPattern] = {

    // Define array of detected patterns
    var detectedPatterns = new ListBuffer[DetectedPattern]()

    // Sizes
    val heightSrc = srcPixels.length
    val widthSrc = srcPixels(0).length
    val heightPtt = patternPixels.length
    val widthPtt = patternPixels(0).length

    // Get first pixel of pattern image
    val firstPixelPattern = patternPixels(0)(0)

    var result = false
    for (i <- 0 to heightSrc - heightPtt) {
      for (j <- 0 to widthSrc -  widthPtt) {
        if (srcPixels(i)(j) == firstPixelPattern) { // If we found a pixel in SourceImage with the same value than the first one of pattern image
          result = testPattern(i, j, 0, srcPixels, patternPixels)// Then we can try to find the full pattern image
          if(result) {
            var detectP = DetectedPattern(j, i, j + widthPtt, i + heightPtt)
            detectedPatterns += detectP
          }
        }
      }
    }

    return detectedPatterns

  }

}
