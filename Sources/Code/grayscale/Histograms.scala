package grayscale

import caseClasses.{Area, DetectedPattern, Pixel}
import org.jfree.chart.{ChartFactory, ChartFrame, JFreeChart}
import org.jfree.chart.plot.PlotOrientation
import org.jfree.chart.renderer.xy.XYSplineRenderer
import org.jfree.data.xy.{XYSeries, XYSeriesCollection}

import scala.collection.mutable.ListBuffer

object Histograms extends App {


  /**
    * Used to get all the histograms for ares of the source image
    * which first pixel corresponds to one of the pixel corner of pattern image
    * @param imagePixels Pixels of the source image
    * @param patternPixels Pixels of the pattern image
    * @param normalized Boolean to normalize the histogram or not
    * @return All the histograms of the areas wanted
    */
  def getAllHistogramsPixelMatchPattern(imagePixels: Array[Array[Int]], patternPixels: Array[Array[Int]], normalized: Boolean):ListBuffer[Map[Area, Array[Double]]] = {

    var histograms = new ListBuffer[Map[Area, Array[Double]]]()
    val heightSource = imagePixels.length
    val widthSource = imagePixels(0).length
    val heightPattern = patternPixels.length
    val widthPattern = patternPixels(0).length

    // Detect match with the first pixel of the pattern
    for(i <- imagePixels.indices) {
      for(j <- imagePixels(0).indices) {
        if(((imagePixels(i)(j) == patternPixels(0)(0)) && (i < heightSource-heightPattern) && (j < widthSource-widthPattern)) // Top left corner
          || ((imagePixels(i)(j) == patternPixels(0)(widthPattern-1)) && (i < heightSource-widthPattern) && (j < widthSource-heightPattern)) // Top right corner (rotation 90° to the left)
          || ((imagePixels(i)(j) == patternPixels(heightPattern-1)(0)) && (i < heightSource-widthPattern) && (j < widthSource-heightPattern)) // Bottom left corner (rotation 90° to the right)
          || ((imagePixels(i)(j) == patternPixels(heightPattern-1)(widthPattern-1)) && (i < heightSource-heightPattern) && (j < widthSource-widthPattern))) { // Bottom right corner (rotation to 180° to the left)

          var tmpHeightPattern = heightPattern
          var tmpWidthPattern = widthPattern
          var reversed = false

          // Top right corner or Bottom left corner (rotation 90°)
          if(imagePixels(i)(j) == patternPixels(0)(widthPattern-1)
              || imagePixels(i)(j) == patternPixels(heightPattern-1)(0)) {
            tmpHeightPattern = widthPattern
            tmpWidthPattern = heightPattern
            reversed = true
          }

          // Isolate a specific area of the source image
          // The size of this area corresponds to the size of the pattern
          val area = Array.ofDim[Int](tmpHeightPattern, tmpWidthPattern)
          for(k <- 0 until tmpHeightPattern) {
            for(l <- 0 until tmpWidthPattern) {
              area(k)(l) = imagePixels(i+k)(j+l)
            }
          }
          val pixel = Pixel(j,i)
          histograms += Map(Area(pixel,reversed) -> getHistogramValues(area, normalized))
        }
      }
    }

    println("Number of Histograms generated: " + histograms.length)
    return histograms

  }


  /**
    * Used to generate an array with grayscale repartition for the histogram
    * @param imagePixels Pixels of the source image
    * @param normalized Boolean to normalize the histogram or not
    * @return Array with repartition
    */
  def getHistogramValues(imagePixels: Array[Array[Int]], normalized: Boolean):Array[Double] = {

    var histogram = new Array[Double](256)
    val height = imagePixels.length
    val width = imagePixels(0).length

    for(i <- 0 until height) {
      for(j <- 0 until width) {
        val hexa = imagePixels(i)(j).toHexString
        val twoLastHexa = hexa takeRight 2
        val grayLevel = Integer.parseInt(twoLastHexa,16)
        histogram(grayLevel) += 1
      }
    }

    if(normalized) {
      var total = height*width
      for(i <- histogram.indices) {
        histogram(i) = histogram(i)/total
      }
    }

    return histogram

  }


  /**
    * Used to detect patterns with generated histograms
    * @param histograms Histograms to analyze
    * @param patternHisto Histogram of the pattern
    * @param widthPattern Width of the pattern
    * @param heightPattern Height of the pattern
    * @param method Method of comparison to find patterns
    * @return
    */
  def detectPatternsByHistograms(histograms: ListBuffer[Map[Area, Array[Double]]], patternHisto: Array[Double], widthPattern: Int, heightPattern: Int, method: Int):ListBuffer[DetectedPattern] = {

    var detectedPatterns = new ListBuffer[DetectedPattern]

    for(i <- histograms.indices) {
      for ((k,v) <- histograms(i)) {

        val pixelX1 = k.pixel.x
        val pixelY1 = k.pixel.y
        var pixelX2 = pixelX1+widthPattern
        var pixelY2 = pixelY1+heightPattern

        if(k.reversed) {
          pixelX2 = pixelX1+heightPattern
          pixelY2 = pixelY1+widthPattern
        }

        var identical = false

        method match {
          case 1 => identical = compareHistogramsBhattacharyyaMethod(v, patternHisto)
          case 2 => identical = compareHistogramIntersectionMethod(v, patternHisto)
          case 3 => identical = compareHistogramChiSquareMethod(v, patternHisto)
          case 4 => identical = compareHistogramCorrelationMethod(v, patternHisto)
          case _ => identical = compareHistogramsBhattacharyyaMethod(v, patternHisto)
        }

        if(identical) {
          detectedPatterns += DetectedPattern(pixelX1, pixelY1, pixelX2, pixelY2)
        }

      }
    }

    return detectedPatterns

  }


  /**
    * Used to compare two histograms of values with the Bhattacharyya Method
    * @param firstHisto First histogram
    * @param secondHisto Second histogram
    * @return Indicate if the histograms are identical
    */
  def compareHistogramsBhattacharyyaMethod(firstHisto: Array[Double], secondHisto: Array[Double]):Boolean = {

    var coefficient:Double = 0.0

    if(firstHisto.length == secondHisto.length) {
      for(i <- firstHisto.indices) {
        coefficient += Math.sqrt(firstHisto(i)*secondHisto(i))
      }
    }

    return coefficient >= 1

  }


  /**
    * Used to compare two histograms of values with the Intersection Method
    * @param firstHisto First histogram
    * @param secondHisto Second histogram
    * @return Indicate if the histograms are identical
    */
  def compareHistogramIntersectionMethod(firstHisto: Array[Double], secondHisto: Array[Double]):Boolean = {

    var sumMin = 0.0
    var sumHistoFirst = 0.0
    var sumHistoSecond = 0.0

    if(firstHisto.length == secondHisto.length) {
      for(i <- firstHisto.indices) {
        sumMin += Math.min(firstHisto(i),secondHisto(i))
        sumHistoFirst += firstHisto(i)
        sumHistoSecond += secondHisto(i)
      }
    }

    val coefficient:Double = sumMin/Math.min(sumHistoFirst,sumHistoSecond)
    return coefficient >= 0.998

  }


  /**
    * Used to compare histograms of values with the Chi-Square Method
    * @param firstHisto First histogram
    * @param secondHisto Second histogram
    * @return Indicate if the histograms are identical
    */
  def compareHistogramChiSquareMethod(firstHisto: Array[Double], secondHisto: Array[Double]):Boolean = {

    var coefficient:Double = 0.0

    if(firstHisto.length == secondHisto.length) {
      for(i <- firstHisto.indices) {
        coefficient += Math.pow(firstHisto(i) - secondHisto(i), 2)
        if(firstHisto(i) != 0) coefficient /= firstHisto(i)
      }
    }

    return coefficient <= 0.002

  }


  /**
    * Used to compare histograms of values with the Correlation Method
    * @param firstHisto First histogram
    * @param secondHisto Second histogram
    * @return Indicate if the histograms are identical
    */
  def compareHistogramCorrelationMethod(firstHisto: Array[Double], secondHisto: Array[Double]):Boolean = {

    var sumNum = 0.0
    var sumDen1 = 0.0
    var sumDen2 = 0.0

    if(firstHisto.length == secondHisto.length) {

      val bins = firstHisto.length
      var notH1 = 0.0
      var notH2 = 0.0

      // Calculate NOT Hk
      for(j <- firstHisto.indices) {
        notH1 += firstHisto(j)
        notH2 += secondHisto(j)
      }

      notH1 /= bins
      notH2 /= bins

      for(i <- firstHisto.indices) {

        // Numerator
        sumNum += (firstHisto(i) - notH1)*(secondHisto(i) - notH2)

        // Denominator
        sumDen1 += Math.pow(firstHisto(i) - notH1,2)
        sumDen2 += Math.pow(secondHisto(i) - notH2,2)

      }
    }

    val coefficient:Double = sumNum/Math.sqrt(sumDen1*sumDen2)
    return coefficient >= 1

  }


  /**
    * Used to display an image histogram
    * @param histogramValues Values to display (repartition)
    * @param curved Boolean to choose the rendering mode (curve or bars)
    */
  def displayHistogram(histogramValues: Array[Double], curved: Boolean):Unit = {

    var series = new XYSeries("Graph")

    // Create serie with all the values
    for(m <- histogramValues.indices) {
      series.add(m,histogramValues(m))
    }

    // Create the chart
    var dataset = new XYSeriesCollection()
    dataset.addSeries(series)
    val chart = getChartHistogram(dataset, curved)
    chart.removeLegend()

    // Display frame
    val frame = new ChartFrame(
      "Histogram Grayscale Values",
      chart
    )
    frame.pack()
    frame.setVisible(true)

  }


  /**
    * Used to create the cart for the histogram according to the rendering mode
    * @param dataset Values to renderer the chart
    * @param curved Rendering mode
    * @return The chart rendered with the values and the specified rendering mode
    */
  private def getChartHistogram(dataset: XYSeriesCollection, curved: Boolean):JFreeChart = {

    // Parameters of the chart
    val title = "Grayscale Histogram"
    val xaxis = "Grayscale Level"
    val yaxis = "Number of Pixels"
    val plot = PlotOrientation.VERTICAL

    if(curved) {

      // Curve
      var chart = ChartFactory.createXYLineChart(title, xaxis, yaxis, dataset, plot, true, true, false)
      var spline = new XYSplineRenderer()
      chart.getXYPlot.setRenderer(spline)
      spline.setShapesVisible(false)
      chart.removeLegend()

      return chart

    }

    // Bars
    return ChartFactory.createHistogram(title, xaxis, yaxis, dataset, plot, true, true, false)

  }

}
