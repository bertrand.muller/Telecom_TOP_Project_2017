package rgb

import caseClasses.PixelColor
import com.tncy.top.image.ImageWrapper

object GetColor extends App {

  /**
    * Used to get a two-dimensional array containing the unique gray level value for each pixel
    * @param source Two-dimensionnal array with RGB values for each pixel
    * @return Two-dimensionnal array with gray values
    */
  def getRGBLevelsValues(source: Array[Array[Int]]): Array[Array[PixelColor]] = {

    val height = source.length
    val width = source(0).length
    val colorLevels:Array[Array[PixelColor]] = Array.ofDim(height, width)

    for (h <- 0 until height) {
      for (w <- 0 until width) {
        val red = getColorValue(source, h, w,1)
        val green = getColorValue(source, h, w,2)
        val blue = getColorValue(source, h, w,3)
        val pixelColor =  PixelColor(red, green, blue)
        colorLevels(h)(w) = pixelColor
      }
    }

    return colorLevels

  }


  /**
    * Used to get the average gray level from RGB values
    * @param source Source image
    * @param sourceHeight Current Y-Axis value for the pixel
    * @param sourceWidth Current X-Axis value for the pixel
    * @return Average gray level
    */
  def getColorValue(source: Array[Array[Int]], sourceHeight: Int, sourceWidth: Int, codeColor: Int): Int = {

    val codeRGB: String = source(sourceHeight)(sourceWidth).toHexString


    if(!codeRGB.equals("0")) {
      val rgb = codeRGB takeRight 6
      codeColor match{
        case 1 => return Integer.parseInt(rgb.substring(0,2), 16)
        case 2 => return Integer.parseInt(rgb.substring(2,4), 16)
        case 3 => return Integer.parseInt(rgb.substring(4,6), 16)
        case _ => return 255
      }
    }

    return 255

  }
}
