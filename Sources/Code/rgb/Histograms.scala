package rgb

import caseClasses.{Area, DetectedPattern, Pixel, PixelColor}
import org.jfree.chart.plot.PlotOrientation
import org.jfree.chart.renderer.xy.XYSplineRenderer
import org.jfree.chart.{ChartFactory, ChartFrame, JFreeChart}
import org.jfree.data.xy.{XYSeries, XYSeriesCollection}
import toolbox.PixColor.comparePixelColor

import scala.collection.mutable.ListBuffer

object  Histograms extends App {


  /**
    * Used to get all the histograms for ares of the source image
    * which first pixel corresponds to one of the corner pixel of pattern image
    * @param imagePixels Pixels of the source image
    * @param patternPixels Pixels of the pattern image
    * @param normalized Boolean to normalize the histograms or not
    * @return All the histograms of the areas wanted
    */
  def getAllHistogramsPixelMatchPattern(imagePixels: Array[Array[PixelColor]], patternPixels: Array[Array[PixelColor]], normalized: Boolean):ListBuffer[Map[Area, Map[String, Array[Double]]]] = {

    var histograms = new ListBuffer[Map[Area, Map[String, Array[Double]]]]()
    val heightSource = imagePixels.length
    val widthSource = imagePixels(0).length
    val heightPattern = patternPixels.length
    val widthPattern = patternPixels(0).length

    // Detect match with the first pixel of the pattern
    for(i <- imagePixels.indices) {
      for(j <- imagePixels(0).indices) {

        val identicalTopLeft = comparePixelColor(imagePixels(i)(j), patternPixels(0)(0))
        val identicalTopRight = comparePixelColor(imagePixels(i)(j), patternPixels(0)(widthPattern-1))
        val identicalBottomLeft = comparePixelColor(imagePixels(i)(j), patternPixels(heightPattern-1)(0))
        val identicalBottomRight = comparePixelColor(imagePixels(i)(j), patternPixels(heightPattern-1)(widthPattern-1))

        if((identicalTopLeft && (i < heightSource-heightPattern) && (j < widthSource-widthPattern)) // Top left corner
          || (identicalTopRight && (i < heightSource-widthPattern) && (j < widthSource-heightPattern)) // Top right corner (rotation 90° to the left)
          || (identicalBottomLeft && (i < heightSource-widthPattern) && (j < widthSource-heightPattern)) // Bottom left corner (rotation 90° to the right)
          || (identicalBottomRight && (i < heightSource-heightPattern) && (j < widthSource-widthPattern))) { // Bottom right corner (rotation to 180° to the left)

          var tmpHeightPattern = heightPattern
          var tmpWidthPattern = widthPattern
          var reversed = false

          // Top right corner or Bottom Left corner (rotation 90°)
          if(identicalTopRight || identicalBottomLeft) {
            tmpHeightPattern = widthPattern
            tmpWidthPattern = heightPattern
            reversed = true
          }

          // Isolate a specific area of the source image
          // The size of this area corresponds to the size of the pattern
          val area = Array.ofDim[PixelColor](tmpHeightPattern, tmpWidthPattern)
          for(k <- 0 until tmpHeightPattern) {
            for(l <- 0 until tmpWidthPattern) {
              area(k)(l) = imagePixels(i+k)(j+l)
            }
          }
          val pixel = Pixel(j,i)
          histograms += Map(Area(pixel,reversed) -> getHistogramValues(area, normalized))
        }
      }
    }

    println("Number of Histograms generated: " + histograms.length)
    return histograms

  }


  /**
    * Used to generate the repartitions for the Red, Green & Blue histograms
    * @param imagePixels Pixels of the source image
    * @param normalized Boolean to normalize the histograms or not
    * @return Array with repartition
    */
  def getHistogramValues(imagePixels: Array[Array[PixelColor]], normalized: Boolean):Map[String, Array[Double]] = {

    var histograms = Map[String, Array[Double]]()
    var histogramRed = new Array[Double](256)
    var histogramGreen = new Array[Double](256)
    var histogramBlue = new Array[Double](256)
    val height = imagePixels.length
    val width = imagePixels(0).length

    // Complete the repartition for each RGB color
    for(i <- 0 until height) {
      for(j <- 0 until width) {
        val pixelColor = imagePixels(i)(j)
        histogramRed(pixelColor.red) += 1
        histogramGreen(pixelColor.green) += 1
        histogramBlue(pixelColor.blue) += 1
      }
    }

    if(normalized) {
      var total = height*width
      for(i <- histogramRed.indices) {
        histogramRed(i) = histogramRed(i)/total
        histogramGreen(i) = histogramGreen(i)/total
        histogramBlue(i) = histogramBlue(i)/total
      }
    }

    histograms += ("red" -> histogramRed)
    histograms += ("green" -> histogramGreen)
    histograms += ("blue" -> histogramBlue)
    return histograms

  }


  /**
    * Used to detect patterns with generated histograms
    * @param histograms Histograms to analyze
    * @param patternHisto Histogram of the pattern
    * @param widthPattern Width of the pattern
    * @param heightPattern Height of the pattern
    * @param method Method of comparison to find patterns
    * @return
    */
  def detectPatternsByHistograms(histograms: ListBuffer[Map[Area, Map[String, Array[Double]]]], patternHisto: Map[String, Array[Double]], widthPattern: Int, heightPattern: Int, method: Int):ListBuffer[DetectedPattern] = {

    var detectedPatterns = new ListBuffer[DetectedPattern]

    for(i <- histograms.indices) {
      for ((area, histogramsRGB) <- histograms(i)) {

        val pixelX1 = area.pixel.x
        val pixelY1 = area.pixel.y
        var pixelX2 = pixelX1+widthPattern
        var pixelY2 = pixelY1+heightPattern

        if(area.reversed) {
          pixelX2 = pixelX1+heightPattern
          pixelY2 = pixelY1+widthPattern
        }

        var identical = false

        method match {
          case 1 => identical = compareHistogramsBhattacharyyaMethod(histogramsRGB, patternHisto)
          case 2 => identical = compareHistogramIntersectionMethod(histogramsRGB, patternHisto)
          case 3 => identical = compareHistogramChiSquareMethod(histogramsRGB, patternHisto)
          /*case 4 => identical = compareHistogramCorrelationMethod(histogramsRGB, patternHisto)*/
          case _ => identical = compareHistogramsBhattacharyyaMethod(histogramsRGB, patternHisto)
        }

        if(identical) {
          detectedPatterns += DetectedPattern(pixelX1, pixelY1, pixelX2, pixelY2)
        }

      }
    }

    return detectedPatterns

  }


  /**
    * Used to compare Red, Green & Blue histograms of values with the Bhattacharyya Method
    * @param firstHisto First histograms
    * @param secondHisto Second histograms
    * @return Indicate if the histograms are identical
    */
  def compareHistogramsBhattacharyyaMethod(firstHisto: Map[String, Array[Double]], secondHisto: Map[String, Array[Double]]):Boolean = {

    // "Red data"
    var coefficientRed:Double = 0.0
    val redHistoFirst = firstHisto("red")
    val redHistoSecond = secondHisto("red")

    // "Green data"
    var coefficientGreen:Double = 0.0
    val greenHistoFirst = firstHisto("green")
    val greenHistoSecond = secondHisto("green")

    // "Blue data"
    var coefficientBlue:Double = 0.0
    val blueHistoFirst = firstHisto("blue")
    val blueHistoSecond = secondHisto("blue")

    if((redHistoFirst.length == redHistoSecond.length)
        && (greenHistoFirst.length == greenHistoSecond.length)
        && (blueHistoFirst.length == blueHistoFirst.length)) {
      for(i <- redHistoFirst.indices) {
        coefficientRed += Math.sqrt(redHistoFirst(i)*redHistoSecond(i))
        coefficientGreen += Math.sqrt(greenHistoFirst(i)*greenHistoSecond(i))
        coefficientBlue += Math.sqrt(blueHistoFirst(i)*blueHistoSecond(i))
      }
    }

    return ((coefficientRed >= 1) && (coefficientGreen >= 1) && (coefficientBlue >= 1))

  }


  /**
    * Used to compare Red, Green & Blue histograms of values with the Intersection Method
    * @param firstHisto First histograms
    * @param secondHisto Second histograms
    * @return Indicate if the histograms are identical
    */
  def compareHistogramIntersectionMethod(firstHisto: Map[String, Array[Double]], secondHisto: Map[String, Array[Double]]):Boolean = {

    // "Red data"
    var sumMinRed = 0.0
    var sumHistoFirstRed = 0.0
    var sumHistoSecondRed = 0.0
    val redHistoFirst = firstHisto("red")
    val redHistoSecond = secondHisto("red")

    // "Green data"
    var sumMinGreen = 0.0
    var sumHistoFirstGreen = 0.0
    var sumHistoSecondGreen = 0.0
    val greenHistoFirst = firstHisto("green")
    val greenHistoSecond = secondHisto("green")

    // "Blue data"
    var sumMinBlue = 0.0
    var sumHistoFirstBlue = 0.0
    var sumHistoSecondBlue = 0.0
    val blueHistoFirst = firstHisto("blue")
    val blueHistoSecond = secondHisto("blue")

    if((redHistoFirst.length == redHistoSecond.length)
      && (greenHistoFirst.length == greenHistoSecond.length)
      && (blueHistoFirst.length == blueHistoFirst.length)) {
      for(i <- redHistoFirst.indices) {

        // Red
        sumMinRed += Math.min(redHistoFirst(i),redHistoSecond(i))
        sumHistoFirstRed += redHistoFirst(i)
        sumHistoSecondRed += redHistoSecond(i)

        // Green
        sumMinGreen += Math.min(greenHistoFirst(i),greenHistoSecond(i))
        sumHistoFirstGreen += greenHistoFirst(i)
        sumHistoSecondGreen += greenHistoSecond(i)

        // Blue
        sumMinBlue += Math.min(blueHistoFirst(i),blueHistoSecond(i))
        sumHistoFirstBlue += blueHistoFirst(i)
        sumHistoSecondBlue += blueHistoSecond(i)

      }
    }

    val coefficientRed:Double = sumMinRed/Math.min(sumHistoFirstRed,sumHistoSecondRed)
    val coefficientGreen:Double = sumMinGreen/Math.min(sumHistoFirstGreen,sumHistoSecondGreen)
    val coefficientBlue:Double = sumMinBlue/Math.min(sumHistoFirstBlue,sumHistoSecondBlue)
    return ((coefficientRed >= 0.998) && (coefficientGreen >= 0.998) && (coefficientBlue >= 0.998))

  }


  /**
    * Used to compare Red, Green & Blue histograms of values with the Chi-Square Method
    * @param firstHisto First histograms
    * @param secondHisto Second histograms
    * @return Indicate if the histograms are identical
    */
  def compareHistogramChiSquareMethod(firstHisto: Map[String, Array[Double]], secondHisto: Map[String, Array[Double]]):Boolean = {

    // "Red data"
    var coefficientRed:Double = 0.0
    val redHistoFirst = firstHisto("red")
    val redHistoSecond = secondHisto("red")

    // "Green data"
    var coefficientGreen:Double = 0.0
    val greenHistoFirst = firstHisto("green")
    val greenHistoSecond = secondHisto("green")

    // "Blue data"
    var coefficientBlue:Double = 0.0
    val blueHistoFirst = firstHisto("blue")
    val blueHistoSecond = secondHisto("blue")

    if((redHistoFirst.length == redHistoSecond.length)
      && (greenHistoFirst.length == greenHistoSecond.length)
      && (blueHistoFirst.length == blueHistoFirst.length)) {
      for(i <- redHistoFirst.indices) {

        // Red
        coefficientRed += Math.pow(redHistoFirst(i) - redHistoSecond(i), 2)
        if(redHistoFirst(i) != 0) coefficientRed /= redHistoFirst(i)

        // Green
        coefficientGreen += Math.pow(greenHistoFirst(i) - greenHistoSecond(i), 2)
        if(greenHistoFirst(i) != 0) coefficientRed /= greenHistoFirst(i)

        // Blue
        coefficientBlue += Math.pow(blueHistoFirst(i) - blueHistoSecond(i), 2)
        if(blueHistoFirst(i) != 0) coefficientRed /= blueHistoFirst(i)

      }
    }

    return ((coefficientRed <= 0.002) && (coefficientGreen <= 0.002) && (coefficientBlue <= 0.002))

  }


  /**
    * Used to compare Red, Green & Blue histograms of values with the Correlation Method
    * @param firstHisto First histograms
    * @param secondHisto Second histograms
    * @return Indicate if the histograms are identical
    */
  def compareHistogramCorrelationMethod(firstHisto: Map[String, Array[Double]], secondHisto: Map[String, Array[Double]]):Boolean = {

    // "Red data"
    val redHistoFirst = firstHisto("red")
    val redHistoSecond = secondHisto("red")
    var sumNumRed = 0.0
    var sumDen1Red = 0.0
    var sumDen2Red = 0.0

    // "Green data"
    val greenHistoFirst = firstHisto("green")
    val greenHistoSecond = secondHisto("green")
    var sumNumGreen = 0.0
    var sumDen1Green = 0.0
    var sumDen2Green = 0.0

    // "Blue data"
    val blueHistoFirst = firstHisto("blue")
    val blueHistoSecond = secondHisto("blue")
    var sumNumBlue = 0.0
    var sumDen1Blue = 0.0
    var sumDen2Blue = 0.0

    if((redHistoFirst.length == redHistoSecond.length)
      && (greenHistoFirst.length == greenHistoSecond.length)
      && (blueHistoFirst.length == blueHistoFirst.length)) {

      // Red
      val binsRed = redHistoFirst.length
      var notH1Red = 0.0
      var notH2Red = 0.0

      // Green
      val binsGreen = greenHistoFirst.length
      var notH1Green = 0.0
      var notH2Green = 0.0

      // Blue
      val binsBlue = blueHistoFirst.length
      var notH1Blue = 0.0
      var notH2Blue = 0.0

      // Calculate NOT Hk
      for(j <- redHistoFirst.indices) {
        notH1Red += redHistoFirst(j)
        notH2Red += redHistoSecond(j)
        notH1Green += greenHistoFirst(j)
        notH2Green += greenHistoSecond(j)
        notH1Blue += blueHistoFirst(j)
        notH2Blue += blueHistoSecond(j)
      }

      notH1Red /= binsRed
      notH2Red /= binsRed
      notH1Green /= binsGreen
      notH2Green /= binsGreen
      notH1Blue /= binsBlue
      notH2Blue /= binsBlue

      for(i <- redHistoFirst.indices) {

        // Numerator
        sumNumRed += (redHistoFirst(i) - notH1Red)*(redHistoSecond(i) - notH2Red)
        sumNumGreen += (greenHistoFirst(i) - notH1Green)*(greenHistoSecond(i) - notH2Green)
        sumNumBlue += (blueHistoFirst(i) - notH1Blue)*(blueHistoSecond(i) - notH2Blue)

        // Denominator
        sumDen1Red += Math.pow(redHistoFirst(i) - notH1Red,2)
        sumDen2Red += Math.pow(redHistoSecond(i) - notH2Red,2)
        sumDen1Green += Math.pow(greenHistoFirst(i) - notH1Red,2)
        sumDen2Green += Math.pow(greenHistoSecond(i) - notH2Red,2)
        sumDen1Blue += Math.pow(blueHistoFirst(i) - notH1Red,2)
        sumDen2Blue += Math.pow(blueHistoSecond(i) - notH2Red,2)

      }
    }

    val coefficientRed:Double = sumNumRed/Math.sqrt(sumDen1Red*sumDen2Red)
    val coefficientGreen:Double = sumNumRed/Math.sqrt(sumDen1Red*sumDen2Red)
    val coefficientBlue:Double = sumNumRed/Math.sqrt(sumDen1Red*sumDen2Red)
    return ((coefficientRed >= 1) && (coefficientGreen >= 1) && (coefficientBlue >= 1))

  }


  /**
    * Used to display Red, Green & Blue histograms on a same chart
    * @param histogramsValues Values for each histogram to display
    * @param curved Boolean to choose the rendering mode (curve or bars)
    */
  def displayHistogram(histogramsValues: Map[String, Array[Double]], curved: Boolean):Unit = {

    val histogramRed = histogramsValues("red")
    var histogramGreen = histogramsValues("green")
    val histogramBlue = histogramsValues("blue")
    var seriesRed = new XYSeries("Graph")
    var seriesGreen = new XYSeries("Graph1")
    var seriesBlue = new XYSeries("Graph2")

    // Create series with all the values
    for(m <- histogramRed.indices) {
      seriesRed.add(m, histogramRed(m))
      seriesBlue.add(m, histogramBlue(m))
      seriesGreen.add(m, histogramGreen(m))
    }

    // Create the chart
    var dataset = new XYSeriesCollection()
    dataset.addSeries(seriesRed)
    dataset.addSeries(seriesBlue)
    dataset.addSeries(seriesGreen)
    val chart = getChartHistogram(dataset, curved)
    chart.removeLegend()

    // Display frame
    val frame = new ChartFrame(
      "Histogram Grayscale Values",
      chart
    )
    frame.pack()
    frame.setVisible(true)

  }


  /**
    * Used to create the cart for the histogram according to the rendering mode
    * @param dataset Values to renderer the chart
    * @param curved Rendering mode
    * @return The chart rendered with the values and the specified rendering mode
    */
  private def getChartHistogram(dataset: XYSeriesCollection, curved: Boolean):JFreeChart = {

    // Parameters of the chart
    val title = "Color Histogram"
    val xaxis = "Color Level"
    val yaxis = "Number of Pixels"
    val plot = PlotOrientation.VERTICAL
    var chart:JFreeChart = null

    if(curved) {

      // Curve
      chart = ChartFactory.createXYLineChart(title, xaxis, yaxis, dataset, plot, true, true, false)
      var spline = new XYSplineRenderer()
      chart.getXYPlot.setRenderer(spline)
      spline.setShapesVisible(false)
      chart.removeLegend()

      return chart

    } else {

      // Bars
      chart = ChartFactory.createHistogram(title, xaxis, yaxis, dataset, plot, true, true, false)
      chart.getXYPlot.setForegroundAlpha(0.6F)

    }

    // Bars
    return chart

  }

}
