import grayscale.Conversion.getGrayLevelValues
import grayscale.Conversion.saveImageInGrayscale
import com.tncy.top.image.ImageWrapper

object Main_Q1 extends App {

  // Source image
  val sampleImage = "test/test01/image.png"
  val wrappedSample = new ImageWrapper(sampleImage)
  val sampleRGBPixels = wrappedSample.getImage()


  // Get gray level values
  val sampleGrayPixels = getGrayLevelValues(sampleRGBPixels)


  // Save image in grayscale
  saveImageInGrayscale(sampleImage,sampleGrayPixels,"outputs/Output_Q1/sampleGreyScales.png")

}
