import com.tncy.top.image.ImageWrapper
import grayscale.Conversion.getGrayLevelValues
import grayscale.IntegralHistograms.getSummedAreaTable
import grayscale.IntegralHistograms.detectPatternNewMethod

object Main_Q6 extends App {

  // Source image
  val sampleImage = "test/test01/image.png"
  val wrappedSample = new ImageWrapper(sampleImage)
  val sampleRGBPixels = wrappedSample.getImage()


  // Pattern image
  val patternImage = "test/test01/imotif.png"
  val wrappedPattern = new ImageWrapper(patternImage)
  val patternRGBPixels = wrappedPattern.getImage()


  // Get grayscale level values
  val sampleGrayPixels = getGrayLevelValues(sampleRGBPixels)
  val patternGrayPixels = getGrayLevelValues(patternRGBPixels)


  // Get summed area for the sample and the pattern
  val summedAreaSample = getSummedAreaTable(sampleGrayPixels)
  val summedAreaPattern = getSummedAreaTable(patternGrayPixels)


  // Calculate summed area table
  detectPatternNewMethod(summedAreaSample, summedAreaPattern)

}
